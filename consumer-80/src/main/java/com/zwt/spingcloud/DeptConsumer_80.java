package com.zwt.spingcloud;

import com.zwt.myrule.zwtRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

/**
 * @author ML李嘉图
 * @version createtime: 2021-10-25
 * Blog: https://www.cnblogs.com/zwtblog/
 */
@SpringBootApplication
@EnableEurekaClient
//time: 2021/10/26
@RibbonClient(name = "Provider",configuration = zwtRule.class)
public class DeptConsumer_80 {
    public static void main(String[] args) {
        SpringApplication.run(DeptConsumer_80.class, args);
    }
}
