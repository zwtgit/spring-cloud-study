package com.zwt.springcloud.service;

import com.zwt.springcloud.pojo.Dept;

import java.util.List;
/**
 * @author ML李嘉图
 * @version createtime: 2021-10-25
 * Blog: https://www.cnblogs.com/zwtblog/
 */
public interface DeptService {
    public boolean addDept(Dept dept);

    public Dept queryDeptById(Long id);

    public List<Dept> queryAll();

}
