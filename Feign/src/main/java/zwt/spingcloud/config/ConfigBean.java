package zwt.spingcloud.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author ML李嘉图
 * @version createtime: 2021-10-25
 * Blog: https://www.cnblogs.com/zwtblog/
 */
@Configuration
public class ConfigBean {
    //    @Configuration----spring applicationContext.xml
    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public IRule MyRule() {
        return new RandomRule();
    }
}
