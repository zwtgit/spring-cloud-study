package com.zwt.service;

import com.zwt.springcloud.pojo.Dept;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author ML李嘉图
 * @version createtime: 2021-10-25
 * Blog: https://www.cnblogs.com/zwtblog/
 */
//time: 2021/10/26
@Component
@FeignClient(fallbackFactory = DeptServiceFallbackFactory.class)
public interface DeptService {
    public boolean addDept(Dept dept);

    public Dept queryDeptById(Long id);

    public List<Dept> queryAll();

}
