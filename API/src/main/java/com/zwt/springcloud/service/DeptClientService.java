package com.zwt.springcloud.service;

import com.zwt.springcloud.pojo.Dept;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author ML李嘉图
 * @version createtime: 2021-10-26
 * Blog: https://www.cnblogs.com/zwtblog/
 */
@Component
@FeignClient(value = "Provider")
public interface DeptClientService {

    @GetMapping("/dept/get/{id}")
    public Dept queryById(@PathVariable("id") Long id);

    @GetMapping("/dept/list")
    public Dept queryAll();

    @GetMapping("/dept/add")
    public Dept add(Dept dept);
}
