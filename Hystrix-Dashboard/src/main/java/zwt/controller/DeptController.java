package zwt.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.zwt.springcloud.pojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import zwt.service.DeptService;

/**
 * @author ML李嘉图
 * @version createtime: 2021-10-26
 * Blog: https://www.cnblogs.com/zwtblog/
 */
@RestController
public class DeptController {

    @Qualifier("deptService")
    @Autowired
    private DeptService service;

    @GetMapping("/dept/get/id")
    @HystrixCommand(fallbackMethod = "hystrixGet")
    public Dept get(@PathVariable("id") Long id) {
        Dept dept = service.queryDeptById(id);

        if (dept == null) {
            throw new RuntimeException("id=>" + id + "不存在，无法获取");
        }

        return dept;
    }

    public Dept Hystrix_get(@PathVariable("id") Long id) {
        return new Dept()
                .setDeptno(id)
                .setDname("id" + id)
                .setDb_source("no");
    }
}
