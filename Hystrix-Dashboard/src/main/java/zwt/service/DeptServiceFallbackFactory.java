package zwt.service;

import com.zwt.springcloud.pojo.Dept;
import com.zwt.springcloud.service.DeptClientService;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author ML李嘉图
 * @version createtime: 2021-10-26
 * Blog: https://www.cnblogs.com/zwtblog/
 */
//降级
@Component
public class DeptServiceFallbackFactory implements FallbackFactory {
    @Override
    public Object create(Throwable throwable) {
        return new DeptClientService() {
            @Override
            public Dept queryById(Long id) {
                return new Dept()
                        .setDeptno(id)
                        .setDname("id" + id)
                        .setDb_source("no");
            }

            @Override
            public Dept queryAll() {
                return null;
            }

            @Override
            public Dept add(Dept dept) {
                return null;
            }
        };
    }
}
